#!/bin/sh -e

#TODO: these sudo and sudo bash prefixes are probably not needed

# install solr
wget http://ai.galib.uga.edu/files/solr-6.6.6.tgz
tar -xvf solr-6.6.6.tgz

# copy solr config
sudo mkdir -p solr-6.6.6/server/solr/configsets/ghnp/conf
sudo mkdir -p solr-6.6.6/server/solr/configsets/ghnp/conf/lang
sudo wget https://gitlab.galileo.usg.edu/infra/solr-configs/ghnp/-/raw/master/ghnp/schema.xml -P solr-6.6.6/server/solr/configsets/ghnp/conf
sudo wget https://gitlab.galileo.usg.edu/infra/solr-configs/ghnp/-/raw/master/ghnp/solrconfig.xml -P solr-6.6.6/server/solr/configsets/ghnp/conf
sudo cp solr-6.6.6/server/solr/configsets/basic_configs/conf/lang/stopwords_en.txt solr-6.6.6/server/solr/configsets/ghnp/conf/lang/stopwords_en.txt

# start solr, creating collection
sudo bash solr-6.6.6/bin/solr start -c -force
sudo bash solr-6.6.6/bin/solr create -c ghnp -d ghnp -force
