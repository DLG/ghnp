#!/bin/bash

### Prepare environment, build requirements
export PATH="$PATH:/app/apache2/bin:/usr/local/pgsql/bin"
cd /app/build
sudo apt-get update
sudo apt-get install -y build-essential nfs-common libexpat1-dev libpcre2-dev pcre2-utils libreadline-dev zlib1g-dev libcurl4-gnutls-dev libpcre3-dev libssl-dev

### Build apache
wget https://ai.galib.uga.edu/files/httpd-2.4.46-w-apr.tar.gz
tar xzf httpd-2.4.46-w-apr.tar.gz
cd httpd-2.4.46/
'./configure'  '--prefix=/app/apache2' '--with-apxs2=/app/apache2/bin/apxs' '--enable-so' '--enable-mods-shared=all' '--enable-http' '--enable-rewrite' '--enable-ssl' '--with-included-apr'
make -j`grep -c ^processor /proc/cpuinfo` && sudo make install

#Update paths
sudo sed -i "s/\/usr\/games/\/app\/apache2\/bin:" /etc/environment
sudo sed -i "s/\/usr\/local\/bin/\/usr\/local\/bin:\/app\/apache2\/bin:\/app\/ruby\/2.5.5\/bin/" /etc/sudoers

### Enable apache modules and update configuration as needed
sudo sed -i "s/User daemon/User vagrant/" /app/apache2/conf/httpd.conf
sudo sed -i "s/Group daemon/Group vagrant/" /app/apache2/conf/httpd.conf
sudo sed -i "s/#LoadModule proxy_http_module modules\/mod_proxy_http.so/LoadModule proxy_http_module modules\/mod_proxy_http.so/" /app/apache2/conf/httpd.conf
sudo sed -i "s/#LoadModule proxy_module modules\/mod_proxy.so/LoadModule proxy_module modules\/mod_proxy.so/" /app/apache2/conf/httpd.conf
sudo sed -i "s/#LoadModule ssl_module modules\/mod_ssl.so/LoadModule ssl_module modules\/mod_ssl.so/" /app/apache2/conf/httpd.conf
sudo sed -i "s/#LoadModule expires_module modules\/mod_expires.so/LoadModule expires_module modules\/mod_expires.so/" /app/apache2/conf/httpd.conf
sudo sed -i "s/#LoadModule rewrite_module modules\/mod_rewrite.so/LoadModule rewrite_module modules\/mod_rewrite.so\nLoadModule wsgi_module modules\/mod_wsgi.so/" /app/apache2/conf/httpd.conf

### Load Chronam conf
echo "Include /opt/chronam/conf/vagrant_apache.conf" | sudo tee -a /app/apache2/conf/httpd.conf

### Build mod_wsgi
cd
wget https://ai.galib.uga.edu/files/mod_wsgi-4.5.15.tar.gz
tar xzf mod_wsgi-4.5.15.tar.gz
cd mod_wsgi-4.5.15
./configure --with-apxs=/app/apache2/bin/apxs  --with-python=/opt/chronam/ENV/bin/python
make -j$(nproc)
sudo make install

### Configure wsgi dir
sudo mkdir /app/apache2/run
sudo chown vagrant:vagrant /app/apache2/run

sudo wget https://ai.galib.uga.edu/files/srvfiles/apache2.service -P /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now apache2.service

#closing note on running server
echo "The GHNP Vagrant box is configured.  Visit localhost:5000 on your machine to view the dev environment. If needed you can restart apache by connecting to the box and using systemctl - vagrant ssh / sudo systemctl restart apache2.service"
