(function($) {

    function setModalSeenCookie() {
        var cookieExpDate = (new Date(Date.now() + 180 * 24 * 60 * 60 * 1000)).toUTCString();
        document.cookie = 'harmful_content_modal_seen=1;expires=' + cookieExpDate + ';path=/';
    }

    function init() {
        if (document.cookie.indexOf('harmful_content_modal_seen=1') > -1) return;

        $('.more-harmful-content-info-button').click(setModalSeenCookie);
        $('.harmful-content-modal').modal({
            open: true
        }).on('hide.bs.modal', setModalSeenCookie);
    }

    $(init);

})(jQuery);
