import json, requests

from chronam import settings

try:
    SLACK_KEY = settings.SLACK_KEY
except:
    SLACK_KEY = None

def send_to_slack(message):
    if SLACK_KEY and message:
        json_data = json.dumps({
                'text': message
            })
        requests.post(SLACK_KEY, data=json_data)
