import os

DIRNAME = os.path.abspath(os.path.dirname(__file__))

DEBUG = True 

ADMINS = (
)

MANAGERS = ADMINS

TIME_ZONE = 'America/New_York'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True

MEDIA_ROOT = ''
MEDIA_URL = ''
IIIF_IMAGE_BASE_URL = "https://gahistoricnewspapers.galileo.usg.edu/images/iiif/2/newspapers" 
REDIRECT_IMAGES_TO_IIIF = True


STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

STATIC_URL = '/media/'
STATIC_ROOT = os.path.join(DIRNAME, '.static-media')

ROOT_URLCONF = 'chronam.urls'

LOGGING = {
    'version': 1,
    'handlers': {
        'applogfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(DIRNAME, 'ghnp_debug.log'),
            'maxBytes': 1024 * 1024 * 15,  # 15MB
            'backupCount': 10,
        }
    },
    'loggers': {
            'GHNP': {
                'handlers': ['applogfile',],
                'level': 'DEBUG',
            },
    }
}

DATABASES = {
    # local
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ghnp',
        'USER': 'vagrant',
        'PASSWORD': 'vagrant',
        'HOST': 'localhost',
        'PORT': ''
        }
    }

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'T6PKr3122^wVVE^FeO5vxy*MYy7@Y^pKVk9AQSYt^vimtXtGQlatxWh@w'

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'chronam.core.middleware.TooBusyMiddleware',
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(DIRNAME, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'chronam.core.context_processors.extra_request_info',
                'chronam.core.context_processors.newspaper_info',
		'django_settings_export.settings_export',
            ],
            'debug' : DEBUG,
        },
    },
]


INSTALLED_APPS = (
    'django.contrib.humanize',
    'django.contrib.staticfiles',
    'djcelery',
    'kombu.transport.django',
    'chronam.core',
)

BROKER_URL = 'django://'

THUMBNAIL_WIDTH = 360

DEFAULT_TTL_SECONDS = 86400  # 1 day
PAGE_IMAGE_TTL_SECONDS = 60 * 60 * 24 * 7 * 2  # 2 weeks
API_TTL_SECONDS = 60 * 60  # 1 hour
FEED_TTL_SECONDS = 60 * 60 * 24 * 7

USE_TIFF = False

ESSAYS_FEED = "http://essays.loc.gov/feed/"

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
        'TIMEOUT': 4838400,  # 2 months
    }
}

IS_PRODUCTION = True
CTS_USERNAME = 'username'
CTS_PASSWORD = 'password'
CTS_PROJECT_ID = 'ndnp'
CTS_QUEUE = 'ndnpingestqueue'
CTS_SERVICE_TYPE = 'ingest.NdnpIngest.ingest'
CTS_URL = "https://cts.loc.gov/transfer/"

MAX_BATCHES = 0

PREGEN_THUMBNAILS = True

import multiprocessing
#TOO_BUSY_LOAD_AVERAGE = 1.5 * multiprocessing.cpu_count()
TOO_BUSY_LOAD_AVERAGE = 64

SOLR = "http://localhost:8983/solr/ghnp"
# SOLR = "http://sc0.galib.uga.edu/solr/ghnp"
#SOLR = 'http://solrcloud.galib.uga.edu:8983/solr/ghnp-prod-prime'
SOLR_LANGUAGES = ("eng",)

IIIF = "https://gahistoricnewspapers.galileo.usg.edu/images/iiif/2/newspapers"

STORAGE = '/opt/chronam/data/'
BATCH_STORAGE = os.path.join(STORAGE, "dlg_batches")
EXTRA_METADATA = '/opt/chronam/dlg/ghnp_md/'

# change to point batch_load process at locally-generated marcXML
# BIB_STORAGE = os.path.join(STORAGE, "bib")
BIB_STORAGE = os.path.join(STORAGE,"nonlccn")

OCR_DUMP_STORAGE = os.path.join(STORAGE, "ocr")
COORD_STORAGE = os.path.join(STORAGE, "word_coordinates")


BASE_CRUMBS = [{'label':'Home', 'href': '/'}]

TOPICS_ROOT_URL = 'http://www.loc.gov/rr/news/topics'
TOPICS_SUBJECT_URL = '/topicsSubject.html'

SLACK_KEY = ''

GA_TRACKING_CODE = ''

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
import djcelery
djcelery.setup_loader()

SETTINGS_EXPORT = [
	'GA_TRACKING_CODE'
]
