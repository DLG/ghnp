import os, sys

activate_env="/opt/chronam/ENV/bin/activate_this.py"
execfile(activate_env, dict(__file__=activate_env))

os.environ['CELERY_LOADER'] = 'django'
os.environ['DJANGO_SETTINGS_MODULE'] = "chronam.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

